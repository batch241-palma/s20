console.log("Hello World!");

/* ACTIVITY S20*/

given = Number(prompt("Give me a number"))
console.log("The number you provided is " + given);


for (let number = given; number >= 50; number--, given--) {
	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
	}else if(number % 5 === 0){
		console.log(number)
	}
}

/* STRETCH GOAL */

let wordplay = "supercalifragilisticexpialidocious";
let newWord = "";
console.log(wordplay);

for (w = 0; w < wordplay.length; w++) {


	if( 
		wordplay[w].toLowerCase() == "a" ||
		wordplay[w].toLowerCase() == "e" ||
		wordplay[w].toLowerCase() == "i" ||
		wordplay[w].toLowerCase() == "o" ||
		wordplay[w].toLowerCase() == "u"
		)  { continue; }
	else { newWord += wordplay[w]; }
} 
console.log(newWord);

/* ACTIVITY S20 */